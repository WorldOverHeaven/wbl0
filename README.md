# Wildberries L0

## Запуск

Перед запуском необходимо установить docker и docker-compose
Убедиться, что порты 5433, 4222, 6222, 8222 и 8080 не заняты

1 Клонировать репозиторий   
git clone https://gitlab.com/WorldOverHeaven/wbl0  
2 Перейти в репозиторий   
cd wbl0  
3 Для запуска ввести команду    
docker-compose up --build  
 
Адрес 
localhost:8080

Для получения данных добавить в адресную строку  
/get?id={i}  
Где i - желаемый id

