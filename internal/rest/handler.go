package rest

import (
	"github.com/go-chi/chi"
	"html/template"
	"io"
	"log"
	"nats/internal/adapter"
	"net/http"
)

const path = "/build/view/"

func Handler(a *adapter.Adapter) *chi.Mux {
	router := chi.NewRouter()

	router.Route("/", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			HandlerIndex(w)
		})
		r.Get("/get", a.HandleGet)
	})
	return router
}

func HandlerIndex(out io.Writer) {
	tmpl, err := template.ParseFiles(path + "index.html")
	if err != nil {
		log.Println("tmpl parse error")
		log.Println(err)
		return
	}
	err = tmpl.Execute(out, "")
	if err != nil {
		return
	}
}
