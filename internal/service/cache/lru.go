package cache

import (
	"container/list"
	"nats/internal/model"
	"sync"
)

type Item struct {
	Key   int64
	Value *model.Order
}

type LRU struct {
	capacity int
	items    map[int64]*list.Element
	queue    *list.List
	mu       sync.RWMutex
}

func NewLru(capacity int) *LRU {
	return &LRU{
		capacity: capacity,
		items:    make(map[int64]*list.Element),
		queue:    list.New(),
		mu:       sync.RWMutex{},
	}
}

func (c *LRU) Set(key int64, value *model.Order) bool {
	c.mu.Lock()
	defer c.mu.Unlock()

	if element, exists := c.items[key]; exists {
		c.queue.MoveToFront(element)
		element.Value.(*Item).Value = value
		return true
	}

	if c.queue.Len() == c.capacity {
		c.purge()
	}

	item := &Item{
		Key:   key,
		Value: value,
	}

	element := c.queue.PushFront(item)
	c.items[item.Key] = element

	return true
}

func (c *LRU) SetAll(orders []*model.Order) {
	for _, value := range orders {
		c.Set(value.ID, value)
	}
}

func (c *LRU) purge() {
	if element := c.queue.Back(); element != nil {
		item := c.queue.Remove(element).(*Item)
		delete(c.items, item.Key)
	}
}

func (c *LRU) Get(key int64) *model.Order {
	c.mu.RLock()
	defer c.mu.RUnlock()
	element, exists := c.items[key]
	if !exists {
		return nil
	}
	return element.Value.(*Item).Value
}
