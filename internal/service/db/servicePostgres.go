package db

import (
	"github.com/go-pg/pg/v10"
	"log"
	"nats/internal/model"
)

type Service struct {
	db *pg.DB
}

func NewService(db *pg.DB) *Service {
	return &Service{db: db}
}

func (s *Service) GetByOrderUID(orderUID string) (*model.Order, error) {
	order := &model.Order{OrderUID: orderUID}
	err := s.db.Model(order).Select()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return order, nil
}

func (s *Service) GetByID(id int64) (*model.Order, error) {
	order := &model.Order{ID: id}
	err := s.db.Model(order).WherePK().Select()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return order, nil
}

func (s *Service) GetAll() ([]*model.Order, error) {
	var order []*model.Order
	err := s.db.Model(&order).Select()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return order, nil
}

func (s *Service) Create(order *model.Order) error {
	_, err := s.db.Model(order).Insert()
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
